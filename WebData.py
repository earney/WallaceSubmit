#!/usr/bin/env python3

#return classes the student is enrolled in,
#return the current assignments for each of those classes

import StudentData
import CourseData

class WebData:
  def __init__(self, studentID):
      self._studentID=studentID

      self._student_data=StudentData.StudentData()
      self._course_data=CourseData.CourseData()

  def getCurrentAssignmentData(self):
      _data={}

      _courses=self._student_data.getCourses(self._studentID)

      for _coursename in _courses:
          _data[_coursename]=self._course_data.getCurrentCourseAssignments(_coursename)

      return _data


if __name__ == '__main__':
   _wd=WebData('154317')
   print(_wd.getCurrentAssignmentData())
