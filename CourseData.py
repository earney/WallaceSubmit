#!/usr/bin/env python3
import json
import datetime

import sys

sys.path.append("data")
import vars

class CourseData:
  def __init__(self):
      self._coursedata={}

      with open("data/courses.json") as fp:
         data=json.loads(fp.read())


      for _semester in data['semesters']:
          if _semester['name'] == vars.CurrentSemester:
             self._coursedata=_semester
             return

      #if we got here, we have a problem..  json does not contain the given semester

  def getAllCourses(self):
      _list=[]
      for _d in self._coursedata['courses']:
          _list.append(_d['name'])

      return _list

  def getCurrentCourseAssignments(self, coursename):
      _assignments=[]
      for _d in self._coursedata['courses']:
          if _d['name'] == coursename:
             for _a in _d['assignments']:
                 _begin=datetime.datetime.strptime(_a['BeginDate'], '%Y-%m-%d')
                 _end=datetime.datetime.strptime(_a['DueDate'], '%Y-%m-%d')
                 _now=datetime.datetime.now()
                 if _begin < _now < _end:
                    _assignments.append({'name': _a['name'], 'BeginDate':_a['BeginDate'], 
                                         'DueDate': _a['DueDate']})

      return _assignments


if __name__ == '__main__':
   _cd=CourseData()
   print(_cd.getAllCourses())
   print(_cd.getCurrentCourseAssignments('CIS 150'))
