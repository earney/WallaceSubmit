#!/usr/bin/env python3

import ptyprocess
import select
import time
import shlex

class RunCommand:
  def __init__(self, cmd):
      self._logfile=u''
      self._EOF=False

      self._cmd=cmd
      if isinstance(self._cmd, str):
         self._cmd=shlex.split(self._cmd)

      #we don't want to echo input so set echo=False
      self._proc = ptyprocess.PtyProcessUnicode.spawn(self._cmd, echo=False)
      time.sleep(0.5)

  def getOutput(self):
      return self._logfile

  def read(self):
      while not self._EOF and self._proc.fd in select.select([self._proc.fd],[],[], 1)[0]:
         try:
           _s=self._proc.read()
           print(_s)
           self._logfile+=_s
         except EOFError as e:
           self._EOF=True

  def execute(self):
      #read program intro...
      self.read()

      while not self._EOF:
         #assume that if there is nothing left to read, the user must be prompting for input
         _s=input()
         self._proc.write(u'%s\n' % _s)
         self.read()

         #read output between user prompts...
         self.read()

      self._proc.close()



if __name__ == '__main__':
   _rc=RunCommand('bc')
   _rc.execute()
