import datetime

class CreateHeader:
  def __init__(self, comment=u'//'):
      self._comment=comment
      self._name=''
      self._submitDate=''
      self._assignment=''

  def setName(self, name):
      self._name=name

  def setSubmitDate(self, date=None):
      self._submitDate=date
      if date is None:
         _d=datetime.datetime.now()
         self._submitDate=_d.strftime('%Y-%m-%d %H:%M:%S')

  def setAssignment(self, assignment):
      self._assignment=assignment

  def getHeader(self):
      _str=self._comment + u'#'*60
      _str+='\n'
      _str+=self._comment + '\n'
      _str+=self._comment + ' Assignment: %s\n' % self._assignment
      _str+=self._comment + ' Name: %s\n' % self._name
      _str+=self._comment + ' Submit Date: %s\n' % self._submitDate

      _str+=self._comment + '\n'
      _str+=self._comment + '#'*60
      _str+='\n'

      return _str

if __name__ == '__main__':
   _ch=CreateHeader(comment=u'#')
   _ch.setSubmitDate()
   print(_ch.getHeader())
