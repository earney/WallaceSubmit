#!/usr/bin/env python3

import ptyprocess
import select
import time
import shlex

class CreateHeader:
  def __init__(self, comment=u'//'):
      self._comment=comment
      self._name=''
      self._date=''
      self._assignment=''

  def setName(self, name):
      self._name=name

  def setDate(self, date=None):
      self._date=date
      if date is None:
         self._date=datetime.datetime.now()

  def setAssignment(self, assignment):
      self._assignment=assignment

  def getHeader(self):
      _str=self._comment + u'#'*60
      _str+='\n'

      _str+=self._comment + ' Assignment: %s\n' % self._assignment
      _str+=self._comment + ' Name: %s\n' % self._name
      _str+=self._comment + ' Date: %s\n' % self._date

      _str+=self._comment + '#'*60
      _str+='\n'

      return _str

class RunCommand:
  def __init__(self, cmd):
      self._logfile=u''
      self._EOF=False

      self._cmd=cmd
      if isinstance(self._cmd, str):
         self._cmd=shlex.split(self._cmd)

      #we don't want to echo input so set echo=False
      self._proc = ptyprocess.PtyProcessUnicode.spawn(self._cmd, echo=False)
      time.sleep(0.5)

  def getOutput(self):
      return self._logfile

  def read(self):
      while not self._EOF and self._proc.fd in select.select([self._proc.fd],[],[], 1)[0]:
         try:
           _s=self._proc.read()
           print(_s)
           self._logfile+=_s
         except EOFError as e:
           self._EOF=True

  def execute(self):
      #read program intro...
      self.read()

      while not self._EOF:
         #assume that if there is nothing left to read, the user must be prompting for input
         _s=input()
         self._proc.write(u'%s\n' % _s)
         self.read()

         #read output between user prompts...
         self.read()

      self._proc.close()

_rc=RunCommand('bc')
_rc.execute()

_ch=CreateHeader(comment=u'#')

print(_ch.getHeader())
print(_rc.getOutput())
