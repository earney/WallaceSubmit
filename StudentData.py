#!/usr/bin/env python3
import json

import sys

sys.path.append("data")
import vars

class StudentData:
  def __init__(self):
      self._data={}

      with open("data/students.json") as fp:
         data=json.loads(fp.read())

         for _semester in data['semesters']:
             if _semester['name'] == vars.CurrentSemester:
                self._data=_semester
                return

      #if we got here, we have a problem..  json does not contain the given semester

  def getStudents(self, course):
      for _course in self._data['courses']:
          if _course['name'] == course:
             return _course['students']

      #no course by that name.. raise exception ?

      return []

  def getCourses(self, studentID):
      _courses=[]

      for _course in self._data['courses']:
          for _student in _course['students']:
              if _student['id'] == studentID:
                 _courses.append(_course['name'])

      return _courses


if __name__ == '__main__':
   _sd=StudentData()
   print(_sd.getStudents('CIS 150'))
   print(_sd.getCourses("154317"))
